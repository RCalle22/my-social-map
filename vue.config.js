module.exports = {
  pwa: {
    workboxOptions: {
      skipWaiting: true,
      clientsClaim: true
    },
    name: "Safe Route Mayotte",
    display: "standalone",
    orientation: "portrait",
    themeColor: "#ffffff",
    msTileColor: "#000000",
    iconPaths: {
      msTileImage: "img/icons/mstile-150x150.png"
    }
  },
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false
    }
  },
  transpileDependencies: ["quasar"],
  outputDir: `./server/public/`
};
