import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import "./quasar";
import "leaflet/dist/leaflet.css";
import "leaflet/dist/images/marker-shadow.png";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import "animate.css";
import i18n from "./i18n";

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false;

new Vue({
  i18n,
  render: (h) => h(App)
}).$mount("#app");
