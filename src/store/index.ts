import { Loading } from "@/services/decoration";
import {
  getAllPoints,
  getInitials,
  getVotePoint,
  login,
  logout,
  removePoint,
  setPoint,
  userOnChange,
  votePoint,
} from "@/services/firebase";
import { payme } from "@/services/stripe";
import Vue from "vue";
import Vuex from "vuex";
import { Action, Module, Mutation, VuexModule } from "vuex-class-modules";
import L from "leaflet";

Vue.use(Vuex);

export interface Point {
  id: string;
  uid: string;
  latlng: PointLatLng;
  type: string;
  text: string | null;
  timestamp: number;
}
export interface PointLatLng {
  lat: number;
  lng: number;
}
export interface PointType {
  label: string;
  value: string;
  icon: string;
  class: string;
  color: string;
}
export interface PointVotes {
  pos: number;
  neg: number;
  fin: number;
}
const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
});

@Module
class StoreModule extends VuexModule {
  // state
  uid = "";
  name: string | null = null;
  points: Point[] = [];
  pointsType: { [id: string]: PointType } = {};
  votes: PointVotes = { neg: 0, pos: 0, fin: 0 };
  // getters
  get isLogged() {
    return this.uid !== "";
  }
  get userNameInitials() {
    return getInitials(this.name);
  }

  @Mutation
  setUser({uid, name}: {uid: string, name: string | null}) {
    this.uid = uid;
    this.name = name;
  }
  @Mutation
  setPoints(points: Point[]) {
    this.points = points;
  }
  @Mutation
  setPointsType() {
    this.pointsType  = {
      obs: {
       label: "Obstacle routier",
       value: "obs",
       icon: L.AwesomeMarkers.icon({
         icon: "poo",
         prefix: "fa",
         markerColor: "green",
       }),
       class: "fa fa-poo",
       color: "green"
     },
     acc: {
       label: "Accident",
       value: "acc",
       icon: L.AwesomeMarkers.icon({
         icon: "car-crash",
         prefix: "fa",
         markerColor: "blue",
       }),
       class: "fa fa-car-crash",
       color: "blue"
     },
     cai: {
       label: "Caillassage",
       value: "cai",
       icon: L.AwesomeMarkers.icon({
         icon: "meteor",
         prefix: "fa",
         markerColor: "orange",
       }),
       class: "fa fa-meteor",
       color: "orange"
     },
     com: {
       label: "Route fermée",
       value: "com",
       icon: L.AwesomeMarkers.icon({
         icon: "times",
         prefix: "fa",
         markerColor: "red",
       }),
       class: "fa fa-times",
       color: "red"
     },
     car: {
       label: "Embouteillage",
       value: "car",
       icon: L.AwesomeMarkers.icon({
         icon: "car",
         prefix: "fa",
         markerColor: "grey",
       }),
       class: "fa fa-car",
       color: "grey"
     },
    
   };
  }
  @Mutation
  setVotes(votes: PointVotes) {
    this.votes = votes;
  }
  @Mutation
  clearVotes() {
    this.votes = { neg: 0, pos: 0, fin: 0 };
  }
  @Action
  onSetPointsType() {
    this.setPointsType();
  }
  @Action
  onUserChange() {
    userOnChange(this.setUser);
  }
  // actions
  @Action
  @Loading("check_credentials", null, "incorrect_credentials")
  async login() {
    await login();
  }
  @Action
  @Loading("close_session", null, "close_session_error")
  async loggout() {
    await logout();
  }
  @Action
  @Loading("get_points", null, "network_error")
  async getAllPoints() {
    this.setPoints(await getAllPoints());
  }
  @Action
  @Loading("create_point", null, "network_error")
  async createNewPoint(point: Omit<Point, "id">) {
    const id = await setPoint(point);
    this.setPoints([...this.points, { ...point, id }]);
  }
  @Action
  @Loading("delete_point", null, "network_error")
  async removePoint(id: string) {
    await removePoint(id);
    this.setPoints(this.points.filter((point) => point.id !== id));
  }
  @Action
  @Loading("update_point", "vote_updated", "network_error")
  async updateVote({ point, value }: { point: Point; value: number }) {
    await votePoint(point.id, this.uid, value);
  }
  @Action
  @Loading("get_votes", null, "network_error")
  async getVotes(id: string) {
    this.setVotes(await getVotePoint(id));
  }
  @Action
  @Loading("redirect_to_payment", null, "network_error")
  async payme(value: string) {
    await payme(value);
  }
}

// register module (could be in any file)
export const storeModule = new StoreModule({ store, name: "store" });
