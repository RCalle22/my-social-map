import { loadStripe, Stripe } from "@stripe/stripe-js";

let stripe: Stripe | null = null;
export const PAYMENTS = [
  { label: "5€ ", value: "price_1IDBX3JQFSzIDIMQtkrvBird" },
  { label: "10€ ", value: "price_1IDBXLJQFSzIDIMQ7kzW2cvu" },
  { label: "15€ ", value: "price_1IDBXRJQFSzIDIMQv2IC3kA7" },
  { label: "20€ ", value: "price_1IDBXYJQFSzIDIMQa71uElKy" },
  //   { label: "30€ ", value: "price_1IDBXgJQFSzIDIMQnnDygIEQ" },
  //   { label: "50€ ", value: "price_1IDBXnJQFSzIDIMQGzBzBOJJ" },
  { label: "5€/mes", value: "price_1IDBXxJQFSzIDIMQOqwZf5mD" },
  { label: "10€/mes ", value: "price_1IDBY5JQFSzIDIMQAZ8gaO0C" },
  { label: "15€/mes ", value: "price_1IDBYbJQFSzIDIMQKqwdLNqd" },
  { label: "20€/mes ", value: "price_1IDBYmJQFSzIDIMQIah6rJyj" }
];

async function initStripe() {
  if (!stripe) {
    stripe = await loadStripe(
      "pk_live_51IDBOoJQFSzIDIMQPsH0TNpKnijy7drWtlXjmRUrcrmuqGvKChF8jsWFJBaGVjbiJNhgfQc3gVIlOf8U2UskRPEn00stfpHuKd"
    );
  }
}
export async function payme(priceId: string) {
  await initStripe();
  const result = await stripe?.redirectToCheckout({
    lineItems: [
      {
        price: priceId,
        quantity: 1
      }
    ],
    mode: "payment",
    successUrl: `${window.location.origin}/success`,
    cancelUrl: `${window.location.origin}/error`
  });
  if (result?.error) {
    throw "redirect-to-checkout";
  }
}
