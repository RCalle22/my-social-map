import i18n from "@/i18n";
import { Point, PointType, PointVotes } from "@/store";
import firebase from "firebase";
import { Loading } from "quasar";

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: "AIzaSyB59MxNci8EzeD4KWEZoVjrhrxEhwYhb_8",
  authDomain: "my-social-map.firebaseapp.com",
  projectId: "my-social-map",
  storageBucket: "my-social-map.appspot.com",
  messagingSenderId: "607820408611",
  appId: "1:607820408611:web:ec2380d85e4d7185d30e96",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.analytics();

const pointsDB = firebaseApp.firestore().collection("points");
const typesDB = firebaseApp.firestore().collection("types");
const votesDB = firebaseApp.firestore().collection("votes");
export async function login() {
  return await firebaseApp
    .auth()
    .signInWithPopup(new firebase.auth.GoogleAuthProvider());
}
export async function logout() {
  await firebaseApp.auth().signOut();
}

export const getInitials = (name: string | null) => {
  if (!name) return null;
  const initials = name.split(" ");
  if (initials.length > 1) {
    return (initials[0].charAt(0) + initials[1].charAt(0)).toUpperCase();
  } else {
    return name.substring(0, 2).toUpperCase();
  }
};

export function userOnChange(
  callback: ({ uid, name }: { uid: string; name: string | null }) => void
) {
  Loading.show({ message: i18n.tc("verify_user") });
  firebaseApp.auth().onAuthStateChanged((user) => {
    if (!user) callback({ uid: "", name: null });
    else callback({ uid: user?.uid, name: user?.displayName });
    Loading.hide();
  });
}

export async function getAllTypes() {
  const result = await (await typesDB.get()).docs;
  return result.map((doc) => doc.data() as PointType);
}
export async function getAllPoints() {
  const result = await (await pointsDB.get()).docs;
  return result.map((doc) => {
    return { ...(doc.data() as Point), id: doc.id };
  });
}
export async function setPoint(point: Omit<Point, "id">) {
  return await (await pointsDB.add({ ...point })).id;
}
export async function removePoint(id: string) {
  return await pointsDB.doc(id).delete();
}
export async function getVotePoint(id: string) {
  const result = (await (await votesDB.doc(id).get()).data()) as {
    [uid: string]: number;
  };
  const votes: PointVotes = {
    pos: 0,
    neg: 0,
    fin: 0,
  };
  if (!result) return votes;
  return Object.keys(result).reduce((acc, value) => {
    return {
      pos: acc.pos + (result[value] === 1 ? 1 : 0),
      neg: acc.neg + (result[value] === -1 ? 1 : 0),
      fin: acc.fin + (result[value] === 0 ? 1 : 0),
    };
  }, votes);
}
export async function votePoint(id: string, uid: string, vote: number) {
  await votesDB.doc(id).set({ [uid]: vote }, { mergeFields: [uid] });
}
