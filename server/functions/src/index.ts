import * as functions from "firebase-functions";
import admin = require("firebase-admin");
const firebase = admin.initializeApp();

exports.scheduledFunction = functions.pubsub
  .schedule("every 24 hours")
  .onRun(async () => {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    const result = await firebase
      .firestore()
      .collection("points")
      .where("timestamp", "<", yesterday.getTime())
      .get();
    if (result.empty) return;
    return await Promise.all(result.docs.map((doc) => doc.ref.delete()));
  });

exports.onDeletePoint = functions.firestore
  .document("points/{pId}")
  .onDelete((snap) => {
    return admin
      .firestore()
      .collection("votes")
      .doc(snap.id)
      .delete();
  });
